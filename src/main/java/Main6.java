import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main6 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        String[] tokens = str.split(";");
        int k = Integer.parseInt(tokens[0]);
        boolean[][] data = new boolean[2][k];

        List<AbstractMap.SimpleEntry<Integer, Integer>> coords = Arrays.stream(tokens)
                .skip(1)
                .map(t -> t.split(","))
                .map(tt -> new AbstractMap.SimpleEntry<>(
                        Integer.parseInt(tt[0]) - 1,
                        Integer.parseInt(tt[1]) - 1
                ))
                .peek(e -> data[e.getKey()][e.getValue()] = true)
                .collect(Collectors.toList());

        if (coords.size() % 2 == 1) {
            System.out.println("0");
            return;
        }

        for (int x = 0; x <= 1; x += 1) {
            for (int y = 0; y < k; y += 1) {
                boolean value = data[x][y];
                if (value) {
                    continue;
                }

                // down?
                if (x == 0 && !data[1][y]) {
                    data[0][y] = true;
                    data[1][y] = true;
                    continue;
                }

                // right?
                int r = y + 1;
                if (r < k && !data[x][r]) {
                    data[x][y] = true;
                    data[x][r] = true;
                    continue;
                }

                System.out.println("0");
                return;
            }
        }

        System.out.println("1");
    }
}
