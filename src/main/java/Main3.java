import java.util.Scanner;

public class Main3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        String[] tokens = str.split(",");
        int h = Integer.parseInt(tokens[0]);
        int a = Integer.parseInt(tokens[1]);
        int b = Integer.parseInt(tokens[2]);
        int s = Integer.parseInt(tokens[3]);
        int t = Integer.parseInt(tokens[4]);
        int k = Integer.parseInt(tokens[5]);

        boolean result = true;

        for (int day = 1; day <= s; day += 1) {

            if (day > 1) {
                h += k;
            }

            boolean morning = a <= h && h <= b || b <= h && h <= a;
            h -= t;
            boolean evening = a <= h && h <= b || b <= h && h <= a;
            result = result && morning && evening;
        }

        System.out.println(result ? "1" : "0");
    }
}
