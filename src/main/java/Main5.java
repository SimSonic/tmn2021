import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = "6;5,3,1";
        // str = sc.nextLine();

        String[] tokens = str.split(";");
        int nSquares = Integer.parseInt(tokens[0]);
        List<Integer> wallLengths = Arrays.stream(tokens[1].split(","))
                .map(Integer::parseInt)
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
        int nWalls = wallLengths.size();

        int result = Integer.MAX_VALUE;

        int[] wallSquares = new int[nWalls];
        while (true) {
            boolean finish = true;
            for (int w = 0; w < nWalls; w++) {
                finish = finish && wallSquares[w] == nSquares;
            }
            if (finish) {
                break;
            }

            int posToInc = 0;
            for (int w = 0; w < nWalls; w++) {
                if (wallSquares[w] < nSquares) {
                    break;
                }
                posToInc += 1;
            }
            wallSquares[posToInc] += 1;
            for (int w = 0; w < posToInc; w++) {
                wallSquares[w] = 0;
            }

            // is correct?
            int totalSquares = Arrays.stream(wallSquares).sum();
            boolean hasZero = Arrays.stream(wallSquares).anyMatch(i -> i == 0);
            if (hasZero || totalSquares != nSquares) {
                continue;
            }

            // calc.
            int square = 0;
            for (int w = 0; w < nWalls; w++) {
                square += calcSquare(wallLengths.get(w), wallSquares[w]);
            }

            result = Math.min(result, square);
        }

        System.out.println(result);
    }

    private static int calcSquare(int len, int cnt) {
        if (cnt == 1) {
            return len * len;
        }

        int sum = 0;
        int src_cnt = cnt;

        for (int i = 1; i <= src_cnt - 1; i++) {
            int h = len / cnt;
            // h = h + (len % cnt > 0 ? 1 : 0);
            int sqLen = Math.max(1, h);
            sum += sqLen * sqLen;
            len -= sqLen;
            cnt -= 1;
        }
        sum += len * len;
        return sum;
    }
}
